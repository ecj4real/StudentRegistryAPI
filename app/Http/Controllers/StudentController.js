'use strict'
const Validator = use('Validator');
const Student = use('App/Model/Student');


class StudentController {
    * index(request, response){
        const students = yield Student.all();
        response.json(students)
    }
    
    * create(request, response){
        const validation = yield Validator.validateAll(request.all(), Student.registerRules)
        if(validation.fails()){
            return response.badRequest({err:validation.messages()});
        }
        try{
            const student = yield Student.create({
                firstname:request.input('firstname'),
                lastname:request.input('lastname'),
                sex:request.input('sex'),
                email:request.input('email'),
                age:request.input('age')
            })
//            response.json(student)
            response.json({msg: 'Student Registered Successfully'})
        }
        catch(e){
            response.expectationFailed({msg:['an error occured while registering student', e.message]})
        }
    }
    
    * update(request, response){
        const validation = yield Validator.validateAll(request.all(), Student.registerRules)
        if(validation.fails()){
            return response.badRequest({err:validation.messages()});
        }
        try{
            const id = request.params('id').id
            const student = yield Student.find(id)
            console.log(student)
            student.firstname = request.input('firstname')
            student.lastname = request.input('lastname')
            student.sex = request.input('sex')
            student.email = request.input('email')
            student.age = request.input('age')
            yield student.save();
            
            //response.json({msg: 'Your todo was successfully updated'})
            response.json({studentUpdate: student})
        }
        catch(e){
            response.expectationFailed({msg:['an error occured while updating student record', e.message]})
        }
    }
    
    * delete(request, response){
        try{
            const id = request.params('id').id
            const student = yield Student.find(id)
            console.log(student)
            yield student.delete();
            
            //response.json({msg: 'Your student record was successfully deleted'})
            response.json({studentDeleted: student})
        }
        catch(e){
            response.expectationFailed({msg:['an error occured while deleting student record', e.message]})
        }
    }
    
}

module.exports = StudentController
