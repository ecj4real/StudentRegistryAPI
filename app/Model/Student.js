'use strict'

const Lucid = use('Lucid')

class Student extends Lucid {
    static get registerRules(){
        return{
            firstname: 'required',
            lastname: 'required',
            sex: 'required',
            email: 'required|email',
            age: 'required'
        }
    }
}

module.exports = Student
