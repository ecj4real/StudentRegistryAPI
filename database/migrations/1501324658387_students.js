'use strict'

const Schema = use('Schema')

class StudentTableSchema extends Schema {

  up () {
    this.create('students', (table) => {
      table.increments()
      table.string('firstname')
      table.string('lastname')
      table.string('sex')
      table.string('email')
      table.integer('age')
      table.timestamps()
    })
  }

  down () {
    this.drop('students')
  }

}

module.exports = StudentTableSchema
